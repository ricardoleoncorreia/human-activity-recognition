# Human Activity Recognition

## Summary

To predict Human Activity, there were applied several EDA techniques to
get insights about the problem. 100 of the predictors had more than 95%
missing values and physics knowledge was mandatory to create new features
(vector modules). After high correlated variables were excluded, 15 predictors
were selected to build the machine learning model. A benchmark was created
providing as a start point (naive solution) with only 4 predictors and it had
66% of accuracy. Later Random Forest and Boosting models were applied to all
15 predictors resulting Random Forest as the best model with 96% of accuracy.

The report is part of the projects of the [Data Science Specialization](https://www.coursera.org/specializations/jhu-data-science)
courses at Johns Hopkins University.
